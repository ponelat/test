package wat.factories;

import wat.StoreApiService;
import wat.impl.StoreApiServiceImpl;


public class StoreApiServiceFactory {
    private final static StoreApiService service = new StoreApiServiceImpl();

    public static StoreApiService getStoreApi() {
        return service;
    }
}

package wat.factories;

import wat.PetApiService;
import wat.impl.PetApiServiceImpl;


public class PetApiServiceFactory {
    private final static PetApiService service = new PetApiServiceImpl();

    public static PetApiService getPetApi() {
        return service;
    }
}
